package com.sapient.week1;

import java.util.Scanner;

public class Prime {
     
	public boolean primeornot(int x) {
		
		if(x==0 || x==1) {
			return false; 
		}
		for(int i=2;i<x/2;i++) {
			if(x%i==0) {				
				return false;
			}			
		}
		return true;
	}
	public static void main(String args[]) {
		Prime p= new Prime();
		int d;
		Scanner sc=new Scanner(System.in);
		d=sc.nextInt();
		System.out.println(p.primeornot(d));
	}

}
