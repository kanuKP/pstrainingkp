package com.sapient.week1;

import java.util.Arrays;
import java.util.Scanner;

public class IntegerArray {
	
	public int size;	
	public int [] arr=new int[size];
		IntegerArray() {
			size =10;
			//System.out.println("hello const "+ size);
			arr=new int[size];			
		}
		IntegerArray(int size) {
			this.size=size;
			//System.out.println("hello const 2 "+size);
			arr=new int[size];
					
		}
		IntegerArray(IntegerArray i) {
			arr=i.arr;			
		}
		IntegerArray(int a[]) {
			arr=a;				
		}
	
		public Scanner o=new Scanner(System.in);
		public void read() 
		{		
			System.out.println("Enter the Integers in array :");			
			for(int i=0;i<size;i++) {
				//System.out.println("hello again");
				arr[i]=o.nextInt();			
			}			
		}
		public void print() {
			for(int i=0;i<size;i++) {
				System.out.print(arr[i]+" ");
			}
			
		}
		void sort() {
			Arrays.sort(arr);
			print();
		}
		Double average() {
			Double sum1=0.0;
			for(int i=0;i<size;i++) {
				
				sum1=sum1 + arr[i];
			}
			
			return sum1/size;
		}
		int search(int x) {
			int index= Arrays.binarySearch(arr, x);		
			return index;
		}
		
		public static void main(String args[]) {
			IntegerArray ob=new IntegerArray();
		//	int arr1[]= {1,2,3,4,5,6,7,8};
			ob.read();
			ob.print();
			ob.sort();
		}

}
