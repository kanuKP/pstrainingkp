package com.sapient.week1;

public class Arithmatic {
	// 1. name of method
	// 2. number of args
	// 3. type of args
	// 4. order of args
	// 5. return type is not a part of method signature
	public int add(int a, int b) {
		return a+b;
	}
	public float add(float a, int b) {
		return a+b;
	}
	public float add(int a,float b) {
		return a+b;
	}
	public String add(String a, String b) {
		return a+b;
	}
}
