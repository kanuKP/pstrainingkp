package com.sapient.week1;

public class Controller {
	
	public static void main(String[] args) {
		AddBean addbean=new AddBean();
		View view=new View();
		AddDAO adddao=new AddDAO();
		view.readData(addbean);
		adddao.compute(addbean);
		view.display(addbean);
		addbean=null;
		adddao=null;
		view=null;
		System.gc();
		//view.display(addbean);
	}
	
	

}
