package p1;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.glassfish.jersey.client.ClientConfig;

public class Demo {
	public static void main(String[] args) {
		ClientConfig config = new ClientConfig();
		Client client= ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseUri());
		//System.out.println("hello");
		System.out.println(target.path("rest").path("hello/100/Bella/hadid/38").request().accept(MediaType.TEXT_PLAIN).get(String.class));
	}	
	public static URI getBaseUri() {		
		return UriBuilder.fromUri("http://10.151.60.208:8070/Project1").build();
	} 
}
