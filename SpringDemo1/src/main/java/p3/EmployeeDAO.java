package p3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO<Studentbean> {
	
	public List<EmployeePOJO> getDeatils(int id) throws Exception{
		Connection c = DBConnection.getMyconnection();		 
        PreparedStatement ps = c.prepareStatement("select id,age,name,salary from employee where id=?");
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        List<EmployeePOJO> lo = new ArrayList<EmployeePOJO>();
        EmployeePOJO s = null;
        while(rs.next()) {
             s= new EmployeePOJO(rs.getInt(1), rs.getInt(2), rs.getString(3),rs.getInt(4));
             lo.add(s);
        }
        return lo;		
	}

}
