package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0) //this has to be executed first
public class ArithmaticAspect {
@Before("execution(* *.*(double,double))") //any package,any class,any method, method with double double, execute
	public void check1(JoinPoint jpoint) {
	System.out.println("in before");
		for (Object x : jpoint.getArgs()) {
			double v=(Double) x;
			if(v<0) {
				throw new IllegalArgumentException("Number cannot be negative");
			}
			
		}
	}

@AfterReturning(pointcut = "execution(* *.*(double,double))", 
returning = "retVal")
	public void check2(JoinPoint jpoint, Object retVal) {
	System.out.println("in after");
		//System.out.println(retVal.toString());
		double v=(Double) retVal;
			if(v<0) {
				throw new IllegalArgumentException("Number cannot be negative");
			}
		}
	}


