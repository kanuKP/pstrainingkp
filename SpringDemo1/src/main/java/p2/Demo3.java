package p2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Demo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
		Arithmatic ob;
	//	ob.add(num1, num2);
		ApplicationContext context= new ClassPathXmlApplicationContext("bean.xml");
		ob=(Arithmatic) context.getBean("arithmatic");
		System.out.println(ob.add(5, 6.0));
		System.out.println(ob.sub(5.0, 6));
		System.out.println(ob.mul(5, 6));
	}
		catch(Exception e){
			System.out.println(e.getMessage());
			}
		}

}
