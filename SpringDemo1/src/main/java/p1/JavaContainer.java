package p1;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JavaContainer {
	@Bean
//	@Scope("Singelton")
	public Hello get1() {
		return new Hello();
	}
	@Bean
	public Employee get2() {
		Employee ob;
		ob=new Employee(22,"Kanu Priya");
		return ob;
	}
	@Bean
	public ListofHolidays get3() {
		ListofHolidays ob;
		ob=new ListofHolidays();
		ob.getHolidays().add(new HolidayPOJO("26/01/2019","Republic day"));
		ob.getHolidays().add(new HolidayPOJO("15/08/2019", "Independance  day"));
		return ob;
	}


}
