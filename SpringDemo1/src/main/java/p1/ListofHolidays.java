package p1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ListofHolidays {
	private List<HolidayPOJO> holidays= new ArrayList<HolidayPOJO>();
//	private Set<HolidayPOJO> holiday= new TreeSet<HolidayPOJO>();

	/*public Set<HolidayPOJO> getHoliday() {
		return holiday;
	}

	public void setHoliday(Set<HolidayPOJO> holiday) {
		this.holiday = holiday;
	}

	@Override
	public String toString() {
		return "ListofHolidays [holiday=" + holiday + "]";
	}

	public ListofHolidays(Set<HolidayPOJO> holiday) {
		super();
		this.holiday = holiday;
	}

	public ListofHolidays() {
		super();
		// TODO Auto-generated constructor stub
	}*/

	public ListofHolidays() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<HolidayPOJO> getHolidays() {
		return holidays;
	}

	public void setHolidays(List<HolidayPOJO> holidays) {
		this.holidays = holidays;
	}

	@Override
	public String toString() {
		return "ListofHolidays [holidays=" + holidays + "]";
	}

	public ListofHolidays(List<HolidayPOJO> holidays) {
		super();
		this.holidays = holidays;
	}
	
}
