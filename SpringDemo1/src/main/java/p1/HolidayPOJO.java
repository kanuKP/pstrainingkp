package p1;


public class HolidayPOJO {
	private String date;
	private String hol_name;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getHol_name() {
		return hol_name;
	}
	public void setHol_name(String hol_name) {
		this.hol_name = hol_name;
	}
	@Override
	public String toString() {
		return "HolidayPOJO [date=" + date + ", hol_name=" + hol_name + "]";
	}
	public HolidayPOJO(String date, String hol_name) {
		super();
		this.date = date;
		this.hol_name = hol_name;
	}
	public HolidayPOJO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
