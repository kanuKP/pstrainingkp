package com.sapient.week1;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class Qualification {

	public static void main(String[] args) {
		Map<Integer, Set<String>> m= new LinkedHashMap<Integer, Set<String>>();
		m.put(101, new LinkedHashSet<String>());
		m.get(101).add("B.Tech");
		//m.add(101,"M. Tech");
		m.get(101).add("M.Tech");
		m.put(102, new LinkedHashSet<String>());
		m.get(102).add("B.Com");
		//m.add(101,"M. Tech");
		m.get(102).add("M.Com");
		System.out.println(m);
		System.out.println(m.keySet());
		System.out.println(m.values());

	}

}
