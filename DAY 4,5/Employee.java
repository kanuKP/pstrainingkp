package com.sapient.week1;

import java.util.Comparator;

public class Employee implements Comparable<Employee>{
	//private list<int> l=new list();
	private int empid;
	private String name;
	private int age;
	
	public Employee() {
		super(); 
	}
	public Employee(int empid, String name, int age) {
		super();
		this.empid = empid;
		this.name = name;
		this.age = age;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public int compareTo(Employee o) {
		return this.name.compareTo(o.name);
	}
	@Override
	public  String toString() {
		return empid+" "+name+" "+age;
	}
	
}
