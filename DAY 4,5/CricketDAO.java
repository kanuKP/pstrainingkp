package com.sapient.week1;

import java.sql.*;
import java.util.*;

public class CricketDAO {

	public List<PlayerBean> getPlayers() throws Exception{
        Connection co = DBconnection.getMyconnection();
        PreparedStatement ps = co.prepareStatement("select id,fname,lname,jerseyno from player");
        ResultSet rs = ps.executeQuery();
        List<PlayerBean> lo = new ArrayList<>();
        PlayerBean player = null;
        while(rs.next()) {
             player= new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
             lo.add(player);
        }
        return lo;
}
	
	public List<PlayerBean> getAnyPlayers(int id) throws Exception{
        Connection co = DBconnection.getMyconnection();
        PreparedStatement ps = co.prepareStatement("select id, FirstName,LastName,jerseyno from player where id=?");
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        List<PlayerBean> lo = new ArrayList<>();
        PlayerBean player = null;
        while(rs.next()) {
             player= new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));
             lo.add(player);
        }
        return lo;
}
	
	 public int insertPlayer(PlayerBean ob) throws Exception{
	        Connection co = DBconnection.getMyconnection();
	        PreparedStatement ps = co.prepareStatement("insert into player(id,firstname,lastname,jerseyno) values (?,?,?,?) ");
	        ps.setInt(1, ob.getId());
	        ps.setString(2, ob.getFirstname());
	        ps.setString(3, ob.getLastname());
	        ps.setInt(4, ob.getJerseyno());
	        int rs = ps.executeUpdate();
	        return rs;
	    }
	 
	 public int UpdatePlayer(int id, PlayerBean ob) throws Exception{
	        Connection co = DBconnection.getMyconnection();
	        PreparedStatement ps = co.prepareStatement("update player set firstname=?,lastname=?,jerseyno=? where id=?");
	        ps.setInt(4, id);
	        ps.setString(1, ob.getFirstname());
	        ps.setString(2, ob.getLastname());
	        ps.setInt(3, ob.getJerseyno());
	        int rs = ps.executeUpdate();
	        return rs;
	    }
	 
	 public int DeletePlayer(int id) throws Exception{
	        Connection co = DBconnection.getMyconnection();
	        PreparedStatement ps = co.prepareStatement("delete from player where id=?");
	        ps.setInt(1, id);
	     //   ps.setString(parameterIndex, x);
	        int rs = ps.executeUpdate();
	        co.commit();
	        System.out.println("deleted");
	        return rs;
	    }
	
}
