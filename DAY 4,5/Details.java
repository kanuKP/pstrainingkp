package com.sapient.week1;

import java.util.*;
import java.util.stream.Collectors;

public class Details {
	
	Map<Integer, Employee> m = new HashMap<Integer, Employee>();
	
	public void InsertData(Employee e) {
		m.put(e.getEmpid(),e);
	}
	public Employee getData(Integer key) {
		return m.get(key);
	}
	public List<Employee> getList(int i){
		List<Employee> L1=m.values().stream().collect(Collectors.toList());
		Collections.sort(L1); //(L1,(o,o 1)->o.getName().compareTo(o1.getName()));
		return L1;
	}
}
