package com.sapient.week2;
import java.sql.Connection;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {
	
	public List<Studentbean> getMarks(int regid) throws Exception{
		Connection c = Studentconnection.getMyconnection();		 
        PreparedStatement ps = c.prepareStatement("select regid,Pmarks,Mmarks,Cmarks,percent from student where regid=?");
        ps.setInt(1, regid);
        ResultSet rs = ps.executeQuery();
        List<Studentbean> lo = new ArrayList<>();
        Studentbean s = null;
        while(rs.next()) {
             s= new Studentbean(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getFloat(6));
             lo.add(s);
        }
        return lo;		
	}
	
	public int InsertMarks(Studentbean s) throws Exception{
		Connection co = Studentconnection.getMyconnection();
        PreparedStatement ps = co.prepareStatement("insert into student(regid,name,Pmarks,Mmarks,Cmarks,percent) values (?,?,?,?,?) ");
        ps.setInt(1, s.getRegid() );
        ps.setString(2, s.getName());
        ps.setInt(3, s.getPmarks());
        ps.setInt(4, s.getMmarks());
        ps.setInt(5, s.getCmarks());        
        float pp=(s.getPmarks()+s.getCmarks()+s.getMmarks())/3;
        ps.setFloat(6, pp);
        int rs = ps.executeUpdate();
        return rs;	
	}
	
	public int Updatestudent(int id, Studentbean s) throws Exception{
        Connection co = Studentconnection.getMyconnection();
        PreparedStatement ps = co.prepareStatement("update student set name=?,Pmarks=?,Mmarks=?,Cmarks=? where regid=?");
        ps.setInt(5, id);
        ps.setString(1, s.getName());
        ps.setInt(2, s.getPmarks());
        ps.setInt(3, s.getMmarks());
        ps.setInt(4, s.getCmarks());
        int rs = ps.executeUpdate();
        return rs;
    }
}
