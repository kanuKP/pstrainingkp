package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;

/**
 * Servlet implementation class Test1servlet
 */
public class Test1servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Test1servlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out=response.getWriter();
		
		try {
		out.print("<html><center><body><h1>Addition of two number</h1>");	 
		out.print("<form action='Test1servlet' method='post'>");
		out.print("<br>Enter number 1 <input type= 'text' name='a1' value='0'/>");
		out.print("<br>enter number 2 <input type= 'text' name='a2' value='0'/>");
		out.print("<br>RESULT  <input type= 'text' name='a2' value='0'/>");
		out.print("<br> <input type= 'SUBMIT'  value='ADD'/>");
		out.print("</form></body></center></html>");
		}
		catch(Exception e) {
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		PrintWriter out=response.getWriter();
		
		try {
			int n1=Integer.parseInt(request.getParameter("a1"));
			int n2=Integer.parseInt(request.getParameter("a2"));
			int n3=n1+n2;
		out.print("<html><center><body><h1>Addition of two number</h1>");	 
		out.print("<form action='Test1servlet' method='post'>");
		out.print("<br>Enter number 1 <input type= 'text' name='a1' value='"+n1+"'/>");
		out.print("<br>enter number 2 <input type= 'text' name='a2' value='"+n2+"'/>");
		out.print("<br>RESULT  <input type= 'text' name='a2' value='"+n3+"'/>");
		out.print("<br> <input type= 'submit'  value='add'/>");
		out.print("</form></body></center></html>");
		}
		catch(Exception e) {
			
		}
		
		//doGet(request, response);
	}

}
