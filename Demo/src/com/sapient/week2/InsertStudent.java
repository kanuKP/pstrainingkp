package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class InsertStudent
 */
public class InsertStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
PrintWriter out=response.getWriter();
		
		try {
		out.print("<html><title>New Stdent Entry</title><body><h1>Insert New Student Entry</h1>");	 
		out.print("<form action='InsertStudent' method='post' align='right'>");	
		out.print("<br>Enter ID                <input type= 'text' name='a1' value='' />");
		out.print("<br>Enter Name              <input type= 'text' name='a2' value=''/>");
		out.print("<br>Enter Physics Marks     <input type= 'text' name='a3' value='' />");
		out.print("<br>Enter Mathematics Marks <input type= 'text' name='a4' value='' />");
		out.print("<br>Enter Chemistry Marks   <input type= 'text' name='a5' value='' />");

		//out.print("<br>Percentage % <input type= 'text' name='a6' value=''/>");
		out.print("<br> <input type= 'SUBMIT'  value='ADD'/>");
		out.print("</form></body></html>");
		}
		catch(Exception e) {
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter out=response.getWriter();
		
		try {
			int n1=Integer.parseInt(request.getParameter("a1"));
			String name=request.getParameter("a2");
			int n2=Integer.parseInt(request.getParameter("a3"));
			int n3=Integer.parseInt(request.getParameter("a4"));
			int n4=Integer.parseInt(request.getParameter("a5"));
			//int n5=Integer.parseInt(request.getParameter("a6"));
		
			out.print("<html><title>New Stdent Entry</title><center><body align='right'><h1>Insert New Student Entry</h1>");	 
			out.print("<form action='InsertStudent' method='post' >");
			out.print("<br>ID                 <input type= 'text' name='a1' value='"+n1+"' '/>");
			out.print("<br>Name               <input type= 'text' name='a2' value='"+name+"'/>");
			out.print("<br>Physics Marks      <input type= 'text' name='a3' value='"+n2+"'/>");
			out.print("<br>Mathematics Marks  <input type= 'text' name='a4' value='"+n3+"'/>");
			out.print("<br>Chemistry Marks    <input type= 'text' name='a5' value='"+n4+"'/>");
			float per=(n2+n3+n4)/3;
			out.print("<br>Percentage %             <input type= 'text' name='a6' value='"+per+"'/>");
			out.print("<br> <input type= 'SUBMIT'  value='ADD'/>");
			out.print("</form></body></center></html>");
		}
		catch(Exception e) {
			
		}

}
}
