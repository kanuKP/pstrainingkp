package com.sapient.week2;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Adminpage
 */
public class Adminpage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Adminpage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out=response.getWriter();
		out.print("<html><center><title>Admin Page</title><meta charset=\"utf-8\">\r\n" + 
				"  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n" + 
				"  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\">\r\n" + 
				"  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>\r\n" + 
				"  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js\"></script><body><h1>Welcome to Admin Page</h1>");
		out.print("<form action=\"Adminpage\" method='post'>");
		
		out.print("<ul class='nav nav-tabs'");
		out.print("<li class=\"active\"><a data-toggle=\"tab\" href=\"LoginPage\">Log out</a></li>");
		out.print("<li><a data-toggle=\"tab\" href=\"#menu1\">Insert Student</a></li>");
		out.print("<li><a data-toggle=\"tab\" href=\"#menu2\">List of Student</a></li>");
		out.print("</ul>");
		out.print("<div class=\"tab-content\">");
		out.print("<div id=\"menu1\" class=\"tab-pane fade in active\">");				
		out.print("<h3>Insert Student</h3>");
		out.print("<form action='LoginPage' method='post'>");
	    //out.print("<button type='submit'>Insert New Entry</button>");
	   // if()
	 //   out.print("<button type='button' value='confirm' name='conf' />");
	    
	/*    String x=request.getParameter("conf");
	    out.print(x);
	    if(x != null && x.equals("confirm")) {
	    ServletContext servletContext = getServletContext();
	    RequestDispatcher requestDispatcher = servletContext
	    .getRequestDispatcher("/LoginPage");
	    requestDispatcher.forward(request, response);
	    }*/
	    out.print("</form>");
		out.print(" <button type=\"button\">Insert New Entry</button></div> ");
		out.print("<div id=\"menu2\" class=\"tab-pane fade\">");
		out.print("<h3>List of Student</h3>");
		out.print(" <button type=\"button\">Edit</button> ");
		out.print(" <button type=\"button\">Delete</button> ");
		out.print("</div>");
		out.print("</div>");
		out.print("</form></body></center></html>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		PrintWriter out=response.getWriter();
		out.print("<html><center><title>Admin Page</title>");
		out.print("<body>");
		out.print("<form action='Adminpage' method='post'>");
	/*	Connection c;
		try {
			c = Studentconnection.getMyconnection();
			PreparedStatement ps = c.prepareStatement("select * from student");
			 ResultSet rs = ps.executeQuery();
		        List<Studentbean> lo = new ArrayList<>();
		        Studentbean s = null;
		        while(rs.next()) {
		             s= new Studentbean(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getFloat(6));
		             out.print("<html><center><title>Admin Page</title>");
		     		out.print("<body>");
		     		out.print("");
		     		out.print("<form action='LoginPage' method='post'>");
		             lo.add(s);
		        }     
		        
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		*/         
		out.print("</form></body></center></html>");
		
	}

}
