package com.sapient.week2;

public class Studentbean {
	
	private int regid;
	private String name;
	private int Pmarks;
	private int Mmarks;
	private int Cmarks;
	private float percent;
	
		
	@Override
	public String toString() {
		return "Studentbean [regid=" + regid + ", name=" + name + ", Pmarks=" + Pmarks + ", Mmarks=" + Mmarks
				+ ", Cmarks=" + Cmarks + ", percent=" + percent + "]";
	}
	public Studentbean(int regid, String name, int pmarks, int mmarks, int cmarks, float percent) {
		super();
		this.regid = regid;
		this.name = name;
		Pmarks = pmarks;
		Mmarks = mmarks;
		Cmarks = cmarks;
		this.percent = percent;
	}
	public Studentbean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getRegid() {
		return regid;
	}
	public void setRegid(int regid) {
		this.regid = regid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPmarks() {
		return Pmarks;
	}
	public void setPmarks(int pmarks) {
		Pmarks = pmarks;
	}
	public int getMmarks() {
		return Mmarks;
	}
	public void setMmarks(int mmarks) {
		Mmarks = mmarks;
	}
	public int getCmarks() {
		return Cmarks;
	}
	public void setCmarks(int cmarks) {
		Cmarks = cmarks;
	}
	public float getPercent() {
		return percent;
	}
	public void setPercent(float percent) {
		this.percent = percent;
	}
		
	
	
	

	

}
